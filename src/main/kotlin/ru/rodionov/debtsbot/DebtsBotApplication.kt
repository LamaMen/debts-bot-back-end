package ru.rodionov.debtsbot

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class DebtsBotApplication

fun main(args: Array<String>) {
    runApplication<DebtsBotApplication>(*args)
}
